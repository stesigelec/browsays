package browsays

import (
	"appengine"
	"appengine/datastore"
	"appengine/user"
	"bytes"
	"fmt"
	"net/http"
	"time"
)

//init defines muxing routes
func init() {
	http.HandleFunc("/", helloHandler)
	http.HandleFunc("/admin", adminHandler)
}

//datastore Connection struct
type Connection struct {
	Date       time.Time
	User       string
	IP         string
	RequestURI string
	Referer    string
	Headers    string
}

// connectionsKey returns the datastore key for Connections.
func connectionsKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "Connections", "def_connect", 0, nil)
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	fmt.Fprintf(w, "<h1>Hello, here is the info you sent:</h1>")
	//connection
	fmt.Fprintf(w, "<p><b>Your IP:</b>%s</p>", r.RemoteAddr)
	fmt.Fprintf(w, "<p><b>You requested:</b>%s</p>", r.RequestURI)
	fmt.Fprintf(w, "<p><b>You came from:</b>%s</p>", r.Referer())
	//headers
	fmt.Fprint(w, "<h2>Here are the headers</h2>")
	for k, v := range r.Header {
		fmt.Fprintf(w, "<p><b>%s:</b>%s</p>", k, v)
	}

	//create struct for recording in datastore
	info := Connection{
		Date:       time.Now(),
		User:       "non loggé",
		IP:         r.RemoteAddr,
		RequestURI: r.RequestURI,
		Referer:    r.Referer(),
	}

	//record user if logged
	if u := user.Current(c); u != nil {
		info.User = u.String()
	}
	//record headers
	buf := new(bytes.Buffer)
	err := r.Header.Write(buf)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	info.Headers = buf.String()

	//store in datastore
	key := datastore.NewIncompleteKey(c, "Connexion", connectionsKey(c))
	_, err = datastore.Put(c, key, &info)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/", http.StatusFound)
}

func adminHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := user.Current(c)
	if u == nil {
		url, err := user.LoginURL(c, r.URL.String())
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Location", url)
		w.WriteHeader(http.StatusFound)
		return
	}
	fmt.Fprintf(w, "Hello, %v!", u)
	if u.Admin {
		fmt.Fprintf(w, "Welcome to the admin console, %v!", u)
	}

}
